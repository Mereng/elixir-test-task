# VisitServer

Тестовое задание. 

[Ответы на вопросы](./ANSWERS.md)

## Запуск

* Установка зависимостей - `mix deps.get`
* В файле `config/config.exs` настроить порты для HTTP-сервера и Redis, также указать хост сервера, где находится Redis 
* Запустить - `mix run --no-halt`

## Тесты
Запуск - `mix test`
