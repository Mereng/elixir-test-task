defmodule VisitServer.Errors.BadRequestError do
  @moduledoc """
  Если запрос невалидный - возникает исключение
  """
  defexception message: "", plug_status: 400
end