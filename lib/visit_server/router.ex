defmodule VisitServer.Router do
    use Plug.Router
    use Plug.ErrorHandler
    plug(:match)
    plug(Plug.Parsers, parsers: [:json, :urlencoding], json_decoder: Poison)
    plug(:dispatch)
    post "/visit_links" do
        {status, body} =
            case conn.body_params do
                %{"links" => links} -> {200, process_links(links)}
                _ -> raise (VisitServer.Errors.BadRequestError)
            end
        send_resp(conn, status, body)
    end

    get "/visited_domains" do
        {status, body} = case conn.params do
            %{"from" => from, "to" => to} -> {200, process_get_domains(from, to)}
            _ -> raise (VisitServer.Errors.BadRequestError)
        end
        send_resp(conn, status, body)
    end

    match _ do
        conn
        |> send_resp(404, "Resource not found")
    end

    defp process_links(links) do
        if not is_list(links) do
            raise (VisitServer.Errors.BadRequestError)
        end
        now = DateTime.utc_now |> DateTime.to_unix
        redis_command = ["ZADD", "links"]
        redis_command = links
        |> Enum.reduce(redis_command, fn (x, acc) ->
            uri = URI.parse(x)
            if uri.host == nil do
                raise (VisitServer.Errors.BadRequestError)
            end
            acc ++ [now, uri.host]
        end)
        {:ok, _} = Redix.command(:redix, redis_command)
        Poison.encode!(%{status: "ok"})
    end

    defp process_get_domains(from, to) do
        from = case Integer.parse(from) do
            {i, ""} -> i
            _ -> raise (VisitServer.Errors.BadRequestError)
        end

        to = case Integer.parse(to) do
            {i, ""} -> i
            _ -> raise (VisitServer.Errors.BadRequestError)
        end
        {:ok, domains} = Redix.command(:redix, ["ZRANGEBYSCORE", "links", from, to])

        Poison.encode!(%{domains: domains, status: "ok"})
    end
end