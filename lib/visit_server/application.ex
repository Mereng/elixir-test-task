defmodule VisitServer.Application do

  use Application

  def start(_type, _args) do
    {:ok, [http_port: http_port, redis_host: redis_host, redis_port: redis_port]} =
      Application.fetch_env(:visit_server, __MODULE__)
    children = [
      {Redix, host: redis_host, port: redis_port, name: :redix},
      {Plug.Cowboy, scheme: :http, plug: VisitServer.Router, options: [port: http_port]}
    ]
    opts = [strategy: :one_for_one, name: VisitServer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
