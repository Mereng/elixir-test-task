defmodule VisitServerTest do
  use ExUnit.Case
  use Plug.Test

  alias VisitServer.Router

  @opts Router.init([])

  setup_all do
    %{now_time: DateTime.utc_now() |> DateTime.to_unix()}
  end

  test "add links" do
    conn =
      conn(:post, "/visit_links", Poison.encode!(%{links: [
        "https://stackoverflow.com/questions/30627233/sort-list-elements-in-elixir-lang?rq=1",
        "https://translate.yandex.ru/?from=tabbar",
        "https://vk.com/feed"
      ]}))
      |> put_req_header("content-type", "application/json")
      |> Router.call(@opts)
    assert conn.state == :sent
    assert conn.status == 200
    %{"status" => "ok"} = Poison.decode!(conn.resp_body)
  end

  test "get domains", state do
      conn(:post, "/visit_links", Poison.encode!(%{links: [
         "https://github.com/elixir-plug/plug/blob/master/test/plug/error_handler_test.exs#L36-L67",
         "https://www.youtube.com/",
         "https://mail.yandex.ru/?uid=425576524429#inbox"
      ]}))
      |> put_req_header("content-type", "application/json")
      |> Router.call(@opts)
    conn =
      conn(:get, "/visited_domains?from=#{state[:now_time] - 1000}&to=#{state[:now_time] + 1000}")
        |> Router.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200
      %{"domains" => domains, "status" => "ok"} = Poison.decode!(conn.resp_body)
      assert Enum.member?(domains, "github.com")
      assert Enum.member?(domains, "www.youtube.com")
      assert Enum.member?(domains, "mail.yandex.ru")
  end

  test "add links 400" do
    assert_raise Plug.Conn.WrapperError, "** (VisitServer.Errors.BadRequestError) ", fn ->
      conn(:post, "/visit_links", Poison.encode!(%{links: [
           "https://stackoverflow.com/questions/30627233/sort-list-elements-in-elixir-lang?rq=1",
           "https:///?from=tabbar",
           "https://vk.com/feed"
      ]}))
        |> Router.call(@opts)
    end
  end

  test "get links 400", state do
    assert_raise Plug.Conn.WrapperError, "** (VisitServer.Errors.BadRequestError) ", fn ->
      conn(:get, "/visited_domains?from=#{state[:now_time] - 1000}&to=78945846jfgiofhyio")
        |> Router.call(@opts)
    end
  end
end
