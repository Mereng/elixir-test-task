defmodule VisitServer.MixProject do
  use Mix.Project

  def project do
    [
      app: :visit_server,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {VisitServer.Application, []}
    ]
  end

  defp deps do
    [
      {:cowboy, "~> 2.6.3"},
      {:plug, "~> 1.8.3"},
      {:plug_cowboy, "~> 2.1.0"},
      {:poison, "~> 4.0.1"},
      {:redix, "~> 0.10.2"}
    ]
  end
end
