use Mix.Config
config :visit_server, VisitServer.Application,
       http_port: 8080,
       redis_host: "localhost",
       redis_port: 6379
